Pod::Spec.new do |s|
  s.name         = "PayooSDK"
  s.version      = "1.0.0"
  s.summary      = "PayooSDK is copyright of Payoo Corp."
  s.description  = <<-DESC
  PayooSDK is copyright of Payoo Corp.
                   DESC
  s.homepage     = "https://github.com/tranhieudai13"
  #s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "DaiTran" => "tranhieudai13@gmail.com.com" }
  #s.source = { :git => "https://tranhieudai13@bitbucket.org/tranhieudai13/payoosdk.git", :tag => s.version.to_s }
  s.source = {:git => "https://tranhieudai13@bitbucket.org/tranhieudai13/payoosdk-framework.git"}
  #s.source_files  = "PayooSDK/**/*.{h,m,swift}"
  s.platform      = :ios, 8.0
  s.swift_version = "4.2"
  s.ios.vendored_frameworks = "PayooSDK.framework"
end